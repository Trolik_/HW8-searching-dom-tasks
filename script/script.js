"use strict";
// debugger;

/*1 task*/
console.log('----------------1 task------------------')

let findParagraphElement = document.querySelectorAll('p');
console.log('findParagraphElement:', findParagraphElement);

findParagraphElement.forEach(element => {
    element.style.backgroundColor = '#ff0000';
})



/*2 task*/
console.log('----------------2 task------------------')

let findOptionsListById = document.getElementById('optionsList');
console.log('findOptionsListById:', findOptionsListById);

let findParentElement = findOptionsListById.parentElement;
console.log('findParentElement:', findParentElement);

let findChildNodes = findOptionsListById.children;
console.log('findChildNodes:', findChildNodes);

for (const child of findChildNodes) {
    console.log('NodeName:', child.nodeName)
    console.log('NodeType:', child.nodeType)
}



/*3 task*/
/*
!!!!!! В завданні вказано:
3) Встановіть в якості контента елемента з класом testParagraph наступний параграф
 - <p>This is a paragraph<p/>, але в коді такого немає - є тільки такий Id.
 Скоріш за все тут помилка в завданні ??
*/
console.log('----------------3 task------------------')

let paragraphSwap = document.getElementById('testParagraph');
paragraphSwap = paragraphSwap.innerText = 'This is a paragraph';

console.log('newTestParagraphContent:', paragraphSwap)



/*4 task*/
console.log('----------------4 task------------------')

let getAllLi = document.querySelector('.main-header').querySelectorAll('ul > li');

getAllLi.forEach(element => {
    element.classList.add('nav-item');
})

console.log('addNewClassToLi:', getAllLi);

/*5 task*/
console.log('----------------5 task------------------')

let findAllElemByClass = document.querySelectorAll('.section-title');
console.log('findAllElemByClass:', findAllElemByClass);

findAllElemByClass.forEach(element => {
    element.classList.remove('section-title');
})

console.log('afterClassRemove:', findAllElemByClass);

